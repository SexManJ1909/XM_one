package xm.contact.Pojo;

public class Contact {
    private String cid;
    private String cname;
    private String cemail;
    private String cphone;
    private String cliuyan;


    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCphone() {
        return cphone;
    }

    public void setCphone(String cphone) {
        this.cphone = cphone;
    }

    public String getCliuyan() {
        return cliuyan;
    }

    public void setCliuyan(String cliuyan) {
        this.cliuyan = cliuyan;
    }
}


