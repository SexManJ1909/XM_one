package xm.contact.Mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import xm.contact.Pojo.Contact;

import java.util.List;

@Mapper
public interface ContactMapper {

    @Select("select * from contact where cname like ifnull(concat(#{arg0},'%'),cname)  limit #{arg1},#{arg2}")
    List<Contact> showall(String cname, int page, int limit);

    @Select("select count(*) from contact where cname like ifnull(concat(#{arg0},'%'),cname)")
    int getCount(String cname);
@Insert("insert into contact (cid,cname,cemail,cphone,cliuyan) values(#{cid},#{cname},#{cemail},#{cphone},#{cliuyan})")
int add(Contact contact);
}

