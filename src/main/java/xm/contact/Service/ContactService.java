package xm.contact.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.contact.Mapper.ContactMapper;
import xm.contact.Pojo.Contact;

import java.util.List;
import java.util.UUID;

@Service
public class ContactService {
@Autowired
ContactMapper contactMapper;
Contact contact;


public List<Contact> showall(String cname, int page, int limit){
        cname= ("".equals(cname)?null:cname);
        int begin=(page-1) * limit;
        return contactMapper.showall(cname,begin,limit);
}

public  int getCount(String cname){
                return contactMapper.getCount(cname);
}

public int add(Contact contact){
    contact.setCid(UUID.randomUUID().toString());
    return contactMapper.add(contact);
}




}
