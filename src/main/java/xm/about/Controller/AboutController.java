package xm.about.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.about.Pojo.About;
import xm.about.Service.AboutService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ziti")
@CrossOrigin
public class AboutController {
    @Autowired
    AboutService aboutService;
    @GetMapping("/findone")
    public Map<String,Object> findone(About about) {
        Map<String,Object> map = new HashMap<>();

        List<About> list=aboutService.findone(about);
        map.put("data",list);

        return map;
    }

    @GetMapping("/showall")
    public Map<String,Object> showall(String nr,int page,int limit){
        Map<String,Object> map=new HashMap<>();
        List<About> list=aboutService.showall(nr,page,limit);
        map.put("data",list);
        map.put("code",0);
        map.put("count",aboutService.getCount(nr));
        map.put("nr",nr);
        return map;
    }
    @PostMapping("/update")
    public int update(About about){
        return aboutService.update(about);
    }






}
