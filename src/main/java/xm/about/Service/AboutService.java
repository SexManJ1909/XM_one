package xm.about.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.about.Mapper.AboutMapper;
import xm.about.Pojo.About;

import java.util.List;

@Service
public class AboutService {
@Autowired
AboutMapper aboutMapper;

public List<About> findone(About about){

        return aboutMapper.findone(about);
}

public List<About> showall(String nr,int page,int limit){
        nr= ("".equals(nr)?null:nr);
        int begin=(page-1) * limit;
        return aboutMapper.showall(nr,begin,limit);
}

public  int getCount(String nr){
                return aboutMapper.getCount(nr);
}

public int update(About about){
                return aboutMapper.update(about);
        }




}
