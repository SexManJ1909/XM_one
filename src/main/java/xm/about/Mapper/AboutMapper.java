package xm.about.Mapper;

import org.apache.ibatis.annotations.*;
import xm.about.Pojo.About;
import java.util.List;

@Mapper
public interface AboutMapper {
@Select("select * from about where aid=#{aid}")
    List<About> findone(About about);


    @Select("select * from about where nr like ifnull(concat(#{arg0},'%'),nr)  limit #{arg1},#{arg2}")
    List<About> showall(String nr, int page, int limit);
    @Select("select count(*) from about where nr like ifnull(concat(#{arg0},'%'),nr)")
    int getCount(String nr);
    @Update("update about set nr=#{nr} where aid=#{aid}")
    int update(About about);

}

