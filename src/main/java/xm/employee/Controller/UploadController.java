package xm.employee.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/upload")
public class UploadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);



    @PostMapping("/tu")
    @ResponseBody
    public Map<String,Object> upload(@RequestParam("file") MultipartFile file) {
        Map<String,Object> map = new HashMap<>();
        if (file.isEmpty()) {
            map.put("code",1);
            return map;
        }

        String fileName = file.getOriginalFilename();
        String filePath = "E:/EImagepass/";
        File dest = new File(filePath + fileName);
        String path = "http://localhost:8099/"+ fileName;
        try {
            file.transferTo(dest);
            LOGGER.info("上传成功");
            map.put("code",0);
            map.put("path", path);

        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
        }

        return map;
    }
}

