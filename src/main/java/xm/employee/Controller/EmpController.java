package xm.employee.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.employee.Pojo.Employee;
import xm.employee.Sevice.EmpService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/emp")
@CrossOrigin
public class EmpController {
    @Autowired
    EmpService empService;
    Employee employee;
    @GetMapping("/showall")
    public Map<String,Object> showall(String ename, int page, int limit){
        int begin = (page - 1) * limit;
        ename = ("".equals(ename) ? null : ename);
        List<Employee> list = empService.showall(ename, begin, limit);
        int yeshu=(empService.getCount(ename))%limit;
        Map<String, Object> map = new HashMap<>();
        map.put("pagea",page);
        map.put("limita",limit);
        map.put("ye",yeshu);
        map.put("data", list);
        map.put("code", 0);
        map.put("count", empService.getCount(ename));
        map.put("ename", ename);
        return map;

    }

    @PostMapping("/add")
    public int add(Employee employee){
        employee.setEid(UUID.randomUUID().toString());
        return empService.add(employee);
    }
    @PostMapping("/update")
    public int update(Employee employee){
        return empService.update(employee);
    }
    @DeleteMapping("/delete/{eid}")
    public int delete(@PathVariable String eid){
        return empService.delete(eid);
    }

}

