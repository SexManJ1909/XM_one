package xm.employee.Sevice;

import xm.employee.Pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.employee.Mapper.EmpMapper;

import java.util.List;
import java.util.UUID;

@Service
public class EmpService {
    @Autowired
    EmpMapper empMapper;


    public List<Employee> showall(String ename, int page, int limit) {
        return empMapper.showall(ename,page,limit);
    }
    public int add(Employee employee){

        return empMapper.add(employee);
    }
    public int update(Employee employee){
        return empMapper.update(employee);
    }
    public int delete(String eid){
        return empMapper.delete(eid);
    }
    public int getCount(String ename){
        return empMapper.getCount(ename);}
}
