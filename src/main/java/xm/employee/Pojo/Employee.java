package xm.employee.Pojo;

public class Employee {
    private String eid;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getEsex() {
        return esex;
    }

    public void setEsex(String esex) {
        this.esex = esex;
    }

    public String getEentrytime() {
        return eentrytime;
    }

    public void setEentrytime(String eentrytime) {
        this.eentrytime = eentrytime;
    }

    public String getEphone() {
        return ephone;
    }

    public void setEphone(String ephone) {
        this.ephone = ephone;
    }

    public String getEdept() {
        return edept;
    }

    public void setEdept(String edept) {
        this.edept = edept;
    }

    public String getEbirthday() {
        return ebirthday;
    }

    public void setEbirthday(String ebirthday) {
        this.ebirthday = ebirthday;
    }

    public String getEadress() {
        return eadress;
    }

    public void setEadress(String eadress) {
        this.eadress = eadress;
    }

    public String getEpay() {
        return epay;
    }

    public void setEpay(String epay) {
        this.epay = epay;
    }

    public String getEpost() {
        return epost;
    }

    public void setEpost(String epost) {
        this.epost = epost;
    }

    public String getEimage() {
        return eimage;
    }

    public void setEimage(String eimage) {
        this.eimage = eimage;
    }

    private String ename;
    private String  esex;
    private String  eentrytime;
    private String ephone;
    private String edept;
    private String ebirthday;
    private String eadress;
    private String epay;
    private String  epost;
    private String eimage;
}
