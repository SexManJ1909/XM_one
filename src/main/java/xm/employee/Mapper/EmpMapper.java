package xm.employee.Mapper;

import org.apache.ibatis.annotations.*;
import xm.employee.Pojo.Employee;

import java.util.List;
@Mapper
public interface EmpMapper {
    @Select("select * from employ where ename like ifnull(concat(#{arg0},'%'),ename) limit #{arg1},#{arg2}")
    List<Employee> showall(String ename, int page, int limit);
    @Select("select count(*) from employ where ename like ifnull(concat(#{arg0},'%'),ename)")
    int getCount(String ename);
    @Insert("insert into employ (eid,ename,esex,eentrytime,ephone,edept,ebirthday,eadress,epay,epost,eimage) values(#{eid},#{ename},#{esex},#{eentrytime},#{ephone},#{edept},#{ebirthday},#{eadress},#{epay},#{epost},#{eimage})")
    int add(Employee employee);
    @Update("update employ set ename=#{ename},esex=#{esex},eentrytime=#{eentrytime},ephone=#{ephone},edept=#{edept},ebirthday=#{ebirthday},eadress=#{eadress},epay=#{epay},epost=#{epost},eimage=#{eimage} where eid=#{eid}")
    int update(Employee employee);
    @Delete("delete from employ where eid=#{eid}")
    int delete(String eid);

}
