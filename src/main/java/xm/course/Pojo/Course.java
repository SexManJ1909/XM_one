package xm.course.Pojo;

public class Course {
    private int cid;
    private String cname;
    private String cteacher;
    private String cpay;

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCteacher() {
        return cteacher;
    }

    public void setCteacher(String cteacher) {
        this.cteacher = cteacher;
    }

    public String getCpay() {
        return cpay;
    }

    public void setCpay(String cpay) {
        this.cpay = cpay;
    }

    public String getCstarttime() {
        return cstarttime;
    }

    public void setCstarttime(String cstarttime) {
        this.cstarttime = cstarttime;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    private String cstarttime;
    private String ctime;
}
