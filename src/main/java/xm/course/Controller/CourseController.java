package xm.course.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.course.Pojo.Course;
import xm.course.Service.CourseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/course")
@CrossOrigin
public class CourseController {
@Autowired
    CourseService courseservice;
@GetMapping("/showall")
public Map<String,Object> showall(String cname,int page,int limit){
    Map<String,Object> map=new HashMap<>();
    List<Course> list=courseservice.showall(cname,page,limit);
    map.put("data",list);
    map.put("code",0);
    map.put("count",courseservice.getCount(cname));
    map.put("cname",cname);
    return map;
}
@PostMapping("/add")
    public int add(Course course){
    return courseservice.add(course);
}
@PostMapping("/update")
    public int update(Course course){
        return courseservice.update(course);
}
@DeleteMapping("/delete/{cid}")
    public int delete(@PathVariable int cid){
        return  courseservice.delete(cid);
}
}
