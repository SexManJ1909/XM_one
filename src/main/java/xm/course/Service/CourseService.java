package xm.course.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.course.Mapper.CourseMapper;
import xm.course.Pojo.Course;

import java.util.List;

@Service
public class CourseService {
@Autowired
    CourseMapper coursemapper;
public List<Course> showall(String cname,int page,int limit){
    cname=("".equals(cname)?null:cname);
 int begin=(page-1)*limit;
    return coursemapper.showall(cname,begin,limit);
}
public int getCount(String cname){
    return coursemapper.getCount(cname);
}
public int add(Course course){
    return coursemapper.add(course);
}
public int update(Course course){
    return coursemapper.update(course);
}
public int delete(int cid){
    return coursemapper.delete(cid);
}
}
