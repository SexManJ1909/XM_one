package xm.course.Mapper;

import org.apache.ibatis.annotations.*;
import xm.course.Pojo.Course;

import java.util.List;

@Mapper
public interface CourseMapper {
@Select("select * from course where cname like ifnull(concat(#{arg0},'%'),cname) limit #{arg1},#{arg2}")
    List<Course> showall(String cname, int page, int limit);
@Select("select count(*) from course where cname like ifnull(concat(#{arg0},'%'),cname)")
int getCount(String cname);
@Insert("insert into course (cid,cname,cteacher,cpay,cstarttime,ctime) values(#{cid},#{cname},#{cteacher},#{cpay},#{cstarttime},#{ctime})")
int add(Course course);
@Update("update course set cname=#{cname},cteacher=#{cteacher},cpay=#{cpay},cstarttime=#{cstarttime},ctime=#{ctime} where cid=#{cid}")
int update(Course course);
@Delete("delete from course where cid=#{cid}")
    int delete(int cid);
}
