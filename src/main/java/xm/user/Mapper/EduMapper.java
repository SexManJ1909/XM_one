package xm.user.Mapper;

import org.apache.ibatis.annotations.*;
import xm.user.Pojo.Education;

import java.util.List;

@Mapper
public interface EduMapper {
  @Select("select * from user where uname like ifnull(concat(#{arg0},'%'),uname) limit #{arg1},#{arg2}")
  List<Education> showall(String uname,int page,int limit);
  @Select("select * from user where uname=#{uname} and upassword=#{upassword}")
  List<Education> findone(Education edu);
  @Insert("insert into user (uid,uname,upassword,uemail,uphone,umark) values(#{uid},#{uname},#{upassword},#{uemail},#{uphone},#{umark})")
  int add(Education edu);
@Update("update user set uname=#{uname},upassword=#{upassword},uemail=#{uemail},uphone=#{uphone},umark=#{umark} where uid=#{uid}")
int update(Education edu);
@Delete("delete from user where uid=#{uid}")
  int delete(String uid);
@Select("select count(*) from user where uname like ifnull(concat(#{arg0},'%'),uname)")
  int getCount(String uname);
@Select("select url from user_role ur right join role_power rp on ur.roleid=rp.roleid where ur.uname=#{uname} and ur.upassword=#{upassword}")
String getDizhi(Education edu);
@Select("select uid from user where uname=#{uname} and upassword=#{upassword}")
  String getuid(Education education);
}
