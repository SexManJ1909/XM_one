package xm.user.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.user.Pojo.Education;
import xm.user.Service.EduService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@CrossOrigin

public class EduController {
@Autowired
    EduService eduservice;
@GetMapping("/showall")
    public Map<String,Object> showall(String uname, int page, int limit){
  Map<String,Object> map=new HashMap<>();
    List<Education> list=eduservice.showall(uname,page,limit);
    map.put("data",list);
    map.put("code",0);
    map.put("count",eduservice.getCount(uname));
    map.put("uname",uname);
    return map;
    }
    @PostMapping("/getuid")
    public String getuid(Education education){
      return eduservice.getuid(education);
    }
    @PostMapping("/checkpower")
    public String getDizhi(Education edu){
    String dizhi=eduservice.getDizhi(edu);
    return dizhi;
    }
    @PostMapping("/check")
      public int check(Education edu){

        if(eduservice.findone(edu).isEmpty()){
          return 0;
        }else{
          return 1;
        }
      }

  @PostMapping("/add")
  public int add(Education edu){
        return eduservice.add(edu);
  }
  @PostMapping("/update")
    public  int update(Education edu){
      return eduservice.update(edu);
  }
  @DeleteMapping("/delete/{uid}")
    public int delete(@PathVariable String uid){
        return eduservice.delete(uid);
  }
}
