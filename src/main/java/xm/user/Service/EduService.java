package xm.user.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.user.Mapper.EduMapper;
import xm.user.Pojo.Education;

import java.util.List;
import java.util.UUID;

@Service
public class EduService {
   @Autowired
    EduMapper edumapper;
   Education edu;
public String getuid(Education education){
    return edumapper.getuid(education);
}
    public List<Education> showall(String uname,int page,int limit){
        uname=("".equals(uname)?null:uname);
        int begin=(page-1)*limit;
        return edumapper.showall(uname,begin,limit);
   }
   public List<Education> findone(Education edu){

        return edumapper.findone(edu);
   }


   public String getDizhi(Education edu){
       return edumapper.getDizhi(edu);
   }
   public int add(Education edu){
       edu.setUid(UUID.randomUUID().toString());
       return edumapper.add(edu);
   }
   public int update(Education edu){
       return edumapper.update(edu);
   }
   public int delete(String uid){
       return edumapper.delete(uid);
   }
   public int getCount(String ename){
       return edumapper.getCount(ename);
   }
}
