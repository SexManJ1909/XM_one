package xm.dept.Pojo;

public class Dept {
 private int did;
 private String dname;
 private String dleader;
 private String dphone;
 private String dadress;

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDleader() {
        return dleader;
    }

    public void setDleader(String dleader) {
        this.dleader = dleader;
    }

    public String getDphone() {
        return dphone;
    }

    public void setDphone(String dphone) {
        this.dphone = dphone;
    }

    public String getDadress() {
        return dadress;
    }

    public void setDadress(String dadress) {
        this.dadress = dadress;
    }
}
