package xm.dept.Mapper;

import org.apache.ibatis.annotations.*;
import xm.dept.Pojo.Dept;

import java.util.List;

@Mapper
public interface DeptMapper {
@Select("select * from dept where dname like ifnull(concat(#{arg0},'%'),dname) limit #{arg1},#{arg2}")
    List<Dept> showall(String dname,int page,int limit);
@Select("select count(*) from dept where dname like ifnull(concat(#{arg0},'%'),dname)")
int getCount(String dname);
@Insert("insert into dept (did,dname,dleader,dphone,dadress) values(#{did},#{dname},#{dleader},#{dphone},#{dadress})")
int add(Dept dept);
@Delete("delete from dept where did=#{arg0}")
int delete(int did);
@Update("update dept set dname=#{dname},dleader=#{dleader},dphone=#{dphone},dadress=#{dadress} where did=#{did}")
int update(Dept dept);
}

