package xm.dept.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.dept.Pojo.Dept;
import xm.dept.Service.DeptService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dept")
@CrossOrigin
public class DeptController {
    @Autowired
    DeptService deptservice;
@GetMapping("/showall")
    public Map<String,Object> showall(String dname,int page,int limit){
    Map<String,Object> map=new HashMap<>();
    List<Dept> list=deptservice.showall(dname,page,limit);
    map.put("data",list);
    map.put("code",0);
    map.put("count",deptservice.getCount(dname));
    map.put("dname",dname);
    return map;
    }
    @PostMapping("/add")
    public int add(Dept dept){
        return deptservice.add(dept);
    }
   @PostMapping("/update")
    public int update(Dept dept){
        return deptservice.update(dept);
   }
@DeleteMapping("/delete/{did}")
public int delete(@PathVariable int did){
    return deptservice.delete(did);
}
}
