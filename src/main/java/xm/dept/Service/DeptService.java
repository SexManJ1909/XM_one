package xm.dept.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.dept.Mapper.DeptMapper;
import xm.dept.Pojo.Dept;

import java.util.List;

@Service
public class DeptService {
@Autowired
    DeptMapper deptmapper;

public List<Dept> showall(String dname,int page,int limit){
    dname= ("".equals(dname)?null:dname);
    int begin=(page-1) * limit;
        return deptmapper.showall(dname,begin,limit);
    }
    public int add(Dept dept){
return deptmapper.add(dept);
    }
    public  int getCount(String dname){
        return deptmapper.getCount(dname);
    }
public int update(Dept dept){
 return deptmapper.update(dept);
}
public int delete(int did){
    return deptmapper.delete(did);
}
}
