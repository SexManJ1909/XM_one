package xm.work.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.work.Mapper.WorkMapper;
import xm.work.Pojo.Work;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.ceil;

@Service
public class WorkService {
    @Autowired
    WorkMapper workmapper;

    public List<Work> showall(String wname,int page,int limit) {
        return workmapper.showall(wname,page,limit);
    }
public int add(Work work){
    return workmapper.add(work);
}
public int update(Work work){
    return workmapper.update(work);
}
public int delete(String wid){
    return workmapper.delete(wid);
}
public int getCount(String wname){
    return workmapper.getCount(wname);}
}
