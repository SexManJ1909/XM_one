package xm.work.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.work.Pojo.Work;
import xm.work.Service.WorkService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.lang.Math.ceil;

@RestController
@RequestMapping("/work")
@CrossOrigin
public class WorkController {
    @Autowired
    WorkService workservice;
    Work work;
@GetMapping("/showall")
    public Map<String,Object> showall(String wname, int page, int limit){
    int begin = (page - 1) * limit;
    wname = ("".equals(wname) ? null : wname);
    List<Work> list = workservice.showall(wname, begin, limit);
    int yeshu=(workservice.getCount(wname))%limit;
    Map<String, Object> map = new HashMap<>();
   map.put("pagea",page);
   map.put("limita",limit);
    map.put("ye",yeshu);
    map.put("data", list);
    map.put("code", 0);
    map.put("count", workservice.getCount(wname));
    map.put("wname", wname);
    return map;

    }
    @PostMapping("/page")
    public int page(String wname,int page,int limit){
        int  ye=workservice.getCount(wname)/limit;
    int a=   workservice.getCount(wname) % limit;
        int yeshu=0;
    if(a==0) {
            yeshu = ye;
       }
       else if(a>0){
            yeshu=ye+1;
       }
       return yeshu;
       }
    @PostMapping("/add")
    public int add(Work work){
        work.setWid(UUID.randomUUID().toString());
        return workservice.add(work);
    }

    @PostMapping("/update")
    public int update(Work work){
        return workservice.update(work);
    }
    @DeleteMapping("/delete/{wid}")
    public int delete(@PathVariable String wid){
return workservice.delete(wid);
    }

}
