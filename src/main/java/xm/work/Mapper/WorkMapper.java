package xm.work.Mapper;

import org.apache.ibatis.annotations.*;
import xm.work.Pojo.Work;

import java.util.List;

@Mapper
public interface WorkMapper {
@Select("select * from work where wname like ifnull(concat(#{arg0},'%'),wname) limit #{arg1},#{arg2}")
    List<Work> showall(String wname,int page,int limit);
@Select("select count(*) from work where wname like ifnull(concat(#{arg0},'%'),wname)")
int getCount(String wname);
@Insert("insert into work (wid,wname,wtime,wendtime,wmark) values(#{wid},#{wname},#{wtime},#{wendtime},#{wmark})")
int add(Work work);
@Update("update work set wname=#{wname},wtime=#{wtime},wendtime=#{wendtime},wmark=#{wmark} where wid=#{wid}")
int update(Work work);
@Delete("delete from work where wid=#{wid}")
int delete(String wid);
}
