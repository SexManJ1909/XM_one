package xm.power.Mapper;

import org.apache.ibatis.annotations.*;
import xm.power.Pojo.Power;

import java.util.List;

@Mapper
public interface PowerMapper {
    @Select("select * from power where pname like ifnull(concat(#{arg0},'%'),pname) limit #{arg1},#{arg2}")
        List<Power> showall(String pname, int page, int limit);
    @Select("select count(*)  from power where pname like ifnull(concat(#{arg0},'%'),pname) ")
        int getCount(String pname);
    @Insert("insert into power(pid,pname,url,pparentid,pislink,psequence) values(#{pid},#{pname},#{url},#{pparentid},#{pislink},#{psequence}) ")
        int add(Power power);
    @Delete("delete from power where pid=#{arg0} ")
        int delete(int pid);
    @Update("update power set pname=#{pname},url=#{url},pparentid=#{pparentid},pislink=#{pislink},psequence=#{psequence}where pid=#{arg0}")
        int update(Power power);

}
