package xm.power.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.course.Pojo.Course;
import xm.power.Pojo.Power;
import xm.power.Service.PowerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/power")
@CrossOrigin
public class PowerController {
    @Autowired
    PowerService powerService;
    public Map<String,Object> showall(String pname, int page, int limit) {
        Map<String,Object> map=new HashMap<>();
        List<Power> list=powerService.showall(pname,page,limit);
        map.put("data",list);
        map.put("code",0);
        map.put("count",powerService.getCount(pname));
        map.put("pname",pname);
        return map;
    }

    @PostMapping("/add")
    public int add(Power power ){
        return powerService.add(power);
    }
    @PostMapping("/update")
    public int update(Power power){
        return powerService.update(power);
    }
    @DeleteMapping("/delete/{pid}")
    public int delete(@PathVariable int pid){
        return  powerService.delete(pid);
    }

}
