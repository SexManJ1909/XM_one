package xm.power.Service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.power.Mapper.PowerMapper;
import xm.power.Pojo.Power;

import java.util.List;

@Service
public class PowerService {
    @Autowired
    PowerMapper powerMapper;

    public List<Power> showall(String pname ,int page ,int limit){
        pname=("".equals(pname)?null:pname);
        int begin=(page-1)*limit;
        return powerMapper.showall(pname,page,limit);

    }

    public  int getCount(String pname){
        return powerMapper.getCount(pname);

    }
    public int add(Power power){
        return powerMapper.add(power);
    }
    public  int delete(int pid){
        return powerMapper.delete(pid);
    }
    public int update(Power power){
        return  powerMapper.update(power);
    }


}
