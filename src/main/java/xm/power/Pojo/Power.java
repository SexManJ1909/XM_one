package xm.power.Pojo;

public class Power {
    private int pid;
    private String pname;
    private String url;
    private String pparentid;
    private String pislink;
    private String psequence;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPparentid() {
        return pparentid;
    }

    public void setPparentid(String pparentid) {
        this.pparentid = pparentid;
    }

    public String getPislink() {
        return pislink;
    }

    public void setPislink(String pislink) {
        this.pislink = pislink;
    }

    public String getPsequence() {
        return psequence;
    }

    public void setPsequence(String psequence) {
        this.psequence = psequence;
    }
}
