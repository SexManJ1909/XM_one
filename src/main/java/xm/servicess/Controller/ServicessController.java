package xm.servicess.Controller;

import com.alibaba.druid.sql.ast.statement.SQLAlterTableRenameIndex;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xm.servicess.Pojo.Servicess;
import xm.servicess.Service.ServicessService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/miti")
@CrossOrigin
public class ServicessController {
    @Autowired
    ServicessService servicessService;
@GetMapping("/findone")
    public Map<String,Object> findone(Servicess servicess) {
        Map<String,Object> map = new HashMap<>();

        List<Servicess> list=servicessService.findone(servicess);
        map.put("data",list);

        return map;
    }

    @GetMapping("/showall")
    public Map<String,Object> showall(String snr,int page,int limit){
        Map<String,Object> map = new HashMap<>();
        List<Servicess> list = servicessService.showall(snr,page,limit);
        map.put("data",list);
        map.put("code",0);
        map.put("count",servicessService.getCount(snr));
        map.put("snr",snr);
        return map;
    }



    @PostMapping("/update")
    public int update(Servicess servicess){
        return servicessService.update(servicess);
    }

    @Delete("/delete/{sid}")
    public int delete(@PathVariable int sid){
        return servicessService.delete(sid);
    }
}
