package xm.servicess.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xm.servicess.Mapper.ServicessMapper;

import xm.servicess.Pojo.Servicess;

import java.util.List;

@Service
public class ServicessService {
        @Autowired
        ServicessMapper servicessMapper;

        public List<Servicess> findone(Servicess servicess){

                return servicessMapper.findone(servicess);
        }



        public List<Servicess> showall(String snr,int page, int limit){
                int begin=(page-1) * limit;
                snr=("".equals(snr))? null:snr;
                return servicessMapper.showall(snr,begin,limit);
        }



        public int getCount(String snr){
                return servicessMapper.getCount(snr);
        }

        public int update(Servicess servicess){
                return servicessMapper.update(servicess);
        }

        public int delete(int sid){
                return servicessMapper.delete(sid);
        }
}
