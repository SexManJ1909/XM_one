package xm.servicess.Mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import xm.servicess.Pojo.Servicess;

import java.util.List;

@Mapper
public interface ServicessMapper {
    @Select("select * from servicess where sid=#{sid}")
    List<Servicess> findone(Servicess servicess);

    //查询全部
    @Select("select * from servicess where snr like ifnull(concat(#{arg0},'%'),snr) limit #{arg1},#{arg2} ")
    List<Servicess> showall(String snr, int page, int limit);


    @Select("select count(*) from servicess where snr like ifnull(concat(#{arg0},'%'),snr)")
    int getCount(String snr);

    @Update("update servicess set snr=#{snr} where sid=#{sid}")
    int update(Servicess servicess);

    @Delete("delete from servicess where sid=#{arg0}")
    int delete(int sid);

}

